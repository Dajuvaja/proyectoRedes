const express = require('express');
const app = express();
const cors = require('cors');

const {createMail} = require('./send.js');

const port = 7000;

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.send('Funciono')
})

app.post('/send-mail', (request, response) => {
    let {user, email, subject, message} = request.body;
    createMail(user, email, subject, message);
    response.json({status: true});
})

app.listen(port, () => {
    console.log(`http://localhost:${port}`)
})