const apiUrl = 'http://localhost:3030'

document.getElementById('submit').addEventListener('click', async () =>{
    const user = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const subject = document.getElementById('subject').value;
    const message = document.getElementById('message').value;

    if(user == '' || email == '' || subject == '' || message == '') return;

    let data = await post(`${apiUrl}/send-mail`, {user, email, subject, message});
    console.log('esto es data: ', data)
})


const post = (url, body) => {
    return new Promise((resolve) => {
        fetch(url, {
            method: 'POST',
            headers: headers(),
            body: JSON.stringify(body),
        })
        .then(res => res.json())
        .then(data => resolve(data))
    })
}

const headers = () => {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("X-Requested-With", "XMLHttpRequest");
    return headers;
}