const fs = require('fs');
const {exec} = require('child_process');

const createMail = (user, email, subject, message) => {
    let text = `From: ${user}
Subject: ${subject}
    
${message}
`;

    let script = `#!/bin/bash
ssmtp -v ${email} < file.txt`;

    fs.writeFile('file.txt', text, function(err){
        if(err) throw err;
        console.log('True');
    })

    fs.writeFile('script.sh', script, function(err){
        if(err) throw err;
        console.log('Script True')
    })

    setTimeout(() => {
        exec('sh script.sh', (error, stdout, stderr) => {
            console.log(stdout);
            console.log(stderr);
            if(error !== null){
                throw 'exec error: ', error;
            }
        })
    }, 4000);
}

module.exports = {createMail}